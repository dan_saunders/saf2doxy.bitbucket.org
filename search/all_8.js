var searchData=
[
  ['id',['ID',['../class_s_channel.html#ab485b5e61a85ac1719f0450f27b44a25',1,'SChannel']]],
  ['initialize',['initialize',['../class_i_s_algorithm.html#a630b5991727d7dc9450f9f57cf20117c',1,'ISAlgorithm']]],
  ['initperchannelplots1d',['initPerChannelPlots1D',['../class_i_s_algorithm.html#acc461569d6200ea4d3efa3de843e209f',1,'ISAlgorithm']]],
  ['inputdatafilenames',['inputDataFileNames',['../class_s_clipboard.html#a8c62203dd72865bd27a27679f4ac8573',1,'SClipboard']]],
  ['integral',['integral',['../class_s_peak.html#a53080cd0cc2f825f9400c091dafe478f',1,'SPeak']]],
  ['integrate',['integrate',['../class_s_waveform.html#a772b31c957c979450f70f503aa1684b9',1,'SWaveform']]],
  ['isalgorithm',['ISAlgorithm',['../class_i_s_algorithm.html',1,'ISAlgorithm'],['../class_i_s_algorithm.html#a88c0f360987fdbf2bc3e6e5591b233e8',1,'ISAlgorithm::ISAlgorithm()']]],
  ['isalgorithm_2ecpp',['ISAlgorithm.cpp',['../_i_s_algorithm_8cpp.html',1,'']]],
  ['isalgorithm_2eh',['ISAlgorithm.h',['../_i_s_algorithm_8h.html',1,'']]],
  ['isoption',['ISOption',['../class_i_s_option.html',1,'ISOption'],['../class_i_s_option.html#a9ef7d0c9d6fcd1fee61bed3787e83296',1,'ISOption::ISOption()']]]
];
