var searchData=
[
  ['cb',['cb',['../class_s_saffron.html#aba917794619fb48472a24d1cc2a9b8fb',1,'SSaffron::cb()'],['../class_i_s_algorithm.html#a02b013497c46878e7de4573d55a01710',1,'ISAlgorithm::cb()']]],
  ['channel',['channel',['../class_s_detector.html#a0eea390e05bd05b0f5f488ddb25a8759',1,'SDetector::channel()'],['../class_s_peak.html#a3f229752294c1ca29f2fd03ea4fc86be',1,'SPeak::channel()'],['../class_s_waveform.html#afc9ccd397795b31176e56df66754848b',1,'SWaveform::channel()']]],
  ['channels',['channels',['../class_s_detector.html#a7b398839f7471725daeb1f63aec50bf6',1,'SDetector']]],
  ['chunks',['chunks',['../class_s_waveform.html#afb4e1a01beed8cdc56f863834566ea42',1,'SWaveform']]],
  ['clear',['clear',['../class_s_clipboard.html#ada01b517ce3629af8f66b75f5375df79',1,'SClipboard::clear()'],['../class_s_channel.html#a32cc8311b6fc774f67d959d692ca7826',1,'SChannel::clear()']]],
  ['clearblock',['clearBlock',['../class_s_detector.html#aa589769ed2d8a09b4df2e4026b9965d4',1,'SDetector']]],
  ['clearchannels',['clearChannels',['../class_s_detector.html#af8029aad353b2fed3a4cf2e0fc8745ff',1,'SDetector']]],
  ['configset',['configSet',['../class_s_detector.html#afb02fed84c725192174f9fa9158a5420',1,'SDetector']]],
  ['cycle',['cycle',['../class_s_clipboard.html#a1337a4b59eb1a62b33e3adb9e153de28',1,'SClipboard']]]
];
