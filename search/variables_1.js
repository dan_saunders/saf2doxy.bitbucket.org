var searchData=
[
  ['m_5falgorithms',['m_algorithms',['../class_s_saffron.html#a4d8a5f3fcfbb0d3f1cba101f72cb7e4d',1,'SSaffron']]],
  ['m_5famplitude',['m_amplitude',['../class_s_peak.html#a4208b676055d63acc4683adbf3d1c33f',1,'SPeak']]],
  ['m_5fbaseline',['m_baseline',['../class_s_channel.html#abf8e6a51dbdb687f44470f9eb3539e5b',1,'SChannel']]],
  ['m_5fchannel',['m_channel',['../class_s_peak.html#aa1e3e31fe600ad933f3e9fc05c45da55',1,'SPeak::m_channel()'],['../class_s_waveform.html#ac2b799df39553fa677f8db32aac70c77',1,'SWaveform::m_channel()']]],
  ['m_5fchannels',['m_channels',['../class_s_detector.html#a9ee5e57eb72ae2dacf51dd6b1e3a4795',1,'SDetector']]],
  ['m_5fchunks',['m_chunks',['../class_s_waveform.html#ae0c312d97b32d88e3b09f1a42a0d1183',1,'SWaveform']]],
  ['m_5fclipboard',['m_clipboard',['../class_s_saffron.html#ae7732c83090b775237eec3b5511f2ef5',1,'SSaffron::m_clipboard()'],['../class_i_s_algorithm.html#ab31c5b6f942704795474fdf6e9437225',1,'ISAlgorithm::m_clipboard()']]],
  ['m_5fconfigset',['m_configSet',['../class_s_detector.html#a41fce02e80e9e5462ebe4ec4e5f24af1',1,'SDetector']]],
  ['m_5fcycle',['m_cycle',['../class_s_clipboard.html#a5fba88fe6b81640cb8064d840436f651',1,'SClipboard']]],
  ['m_5fdetector',['m_detector',['../class_s_saffron.html#a016f4b91de7e4f2c457f42db35225304',1,'SSaffron::m_detector()'],['../class_i_s_algorithm.html#abe78e397d6133521dd3cd162d520f1e6',1,'ISAlgorithm::m_detector()']]],
  ['m_5fdetectorconfig',['m_detectorConfig',['../class_s_detector.html#a588c0f90c41fab48d6dc7ea47bb3b4c3',1,'SDetector']]],
  ['m_5fhistosfile',['m_histosFile',['../class_s_saffron.html#a91162c898b33fd84bb167df42a9b5e1b',1,'SSaffron']]],
  ['m_5fhistosfilename',['m_histosFileName',['../class_s_saffron.html#a626f01319384386a5cfad686bd07f20c',1,'SSaffron']]],
  ['m_5fhtime',['m_htime',['../class_s_peak.html#a1bbf503470f812e3d654e796092bcfa6',1,'SPeak::m_htime()'],['../class_s_waveform.html#a511ab07893218da111977c42d1d52a6c',1,'SWaveform::m_htime()']]],
  ['m_5finputdatafilenames',['m_inputDataFileNames',['../class_s_clipboard.html#a94caa63d4fc6b3bee3bec5b27aa928df',1,'SClipboard']]],
  ['m_5fintegral',['m_integral',['../class_s_peak.html#ab61b4aec444fe7d908c96e429006fd93',1,'SPeak']]],
  ['m_5fmasked',['m_masked',['../class_s_channel.html#aae1880f657e4f8cedd955da36bdc14b2',1,'SChannel']]],
  ['m_5fname',['m_name',['../class_i_s_option.html#ae7204e53267c18a6a2b443c4c0ff9e21',1,'ISOption::m_name()'],['../class_i_s_algorithm.html#a54903d1fe490fa4e724d3851ffc7f049',1,'ISAlgorithm::m_name()']]],
  ['m_5fnchannels',['m_nChannels',['../class_s_detector.html#ad1a038757f4672cc9506eca34be10120',1,'SDetector']]],
  ['m_5fncyclelimit',['m_nCycleLimit',['../class_s_saffron.html#af37039da0d7a4bba17948ce5e176af92',1,'SSaffron']]],
  ['m_5foptions',['m_options',['../class_i_s_algorithm.html#a63514f25675140b266a2fd01e664106c',1,'ISAlgorithm']]],
  ['m_5foutputextension',['m_outputExtension',['../class_s_saffron.html#adf26d6c29f26441c34b78942c7a6ed2d',1,'SSaffron']]],
  ['m_5fpeaks',['m_peaks',['../class_s_clipboard.html#a62da94ff4aa37f4d517f1bc40ab894c0',1,'SClipboard::m_peaks()'],['../class_s_channel.html#a155c04229321b2d9b6d31c82a567056e',1,'SChannel::m_peaks()']]],
  ['m_5fsafchanindex',['m_safChanIndex',['../class_s_channel.html#aa561def77201fb51617ae2e2d977388c',1,'SChannel']]],
  ['m_5fsamples',['m_samples',['../class_s_waveform.html#aa089f0791a529e4109d98aa4acc5c7a6',1,'SWaveform']]],
  ['m_5fsamplewidthns',['m_sampleWidthNs',['../class_s_channel.html#a79b412f8d278095a054a98378a589858',1,'SChannel']]],
  ['m_5fscriptmode',['m_scriptMode',['../class_s_saffron.html#a40b8aa0fccee1627f7e7fbc9ecf4450c',1,'SSaffron']]],
  ['m_5fstoprun',['m_stopRun',['../class_i_s_algorithm.html#a28bea512a812fe3d29989c307f6f1fd1',1,'ISAlgorithm']]],
  ['m_5ftime',['m_time',['../class_s_peak.html#a4a64d45c338672afc9af4bc1be6bd3e6',1,'SPeak::m_time()'],['../class_s_waveform.html#a8389b79258ca6e3f4cc0c7c05e0be07d',1,'SWaveform::m_time()']]],
  ['m_5ftotexecutiontime',['m_totExecutionTime',['../class_s_saffron.html#a45dd22257ffd86fa45740917e7ac7e3e',1,'SSaffron']]],
  ['m_5fval',['m_val',['../class_s_option_int.html#a524248440e9f9bdfab7ef95a4d953683',1,'SOptionInt::m_val()'],['../class_s_option_str.html#a637638fe5ca82f03c025d4fa6972fdc8',1,'SOptionStr::m_val()'],['../class_s_option_double.html#ad78f9586e060491cb745aa71918d4b77',1,'SOptionDouble::m_val()']]],
  ['m_5fwaveform',['m_waveform',['../class_s_peak.html#a0c5cfd36fd8c257d3ab618ae1a4fd5d2',1,'SPeak']]],
  ['m_5fwaveformchunks',['m_waveformChunks',['../class_s_clipboard.html#a12b977198edde817a1fd258d9618ab80',1,'SClipboard::m_waveformChunks()'],['../class_s_channel.html#abcf5503eaf700a1188517bb8e22ac28b',1,'SChannel::m_waveformChunks()']]],
  ['m_5fwaveforms',['m_waveforms',['../class_s_channel.html#a17a0c286b1a35aec254d1f956f5d9136',1,'SChannel']]],
  ['m_5fxglob',['m_xGlob',['../class_s_channel.html#a9ca0f808602e43f442ff609f8c5d6cf0',1,'SChannel']]],
  ['m_5fxloc',['m_xLoc',['../class_s_channel.html#a2183e42944138b9b46c85c3ecf772ada',1,'SChannel']]],
  ['m_5fyglob',['m_yGlob',['../class_s_channel.html#a1be94fa08e4c2a3a35ef1a4fa8d10cd0',1,'SChannel']]],
  ['m_5fyloc',['m_yLoc',['../class_s_channel.html#a23bf2d1cbfd3b2234ccc2915ef06a104',1,'SChannel']]],
  ['m_5fzglob',['m_zGlob',['../class_s_channel.html#adb7a95a83235e1dbd16debd58ab9a85d',1,'SChannel']]],
  ['m_5fzloc',['m_zLoc',['../class_s_channel.html#aa08ddc471a75686ab7b586a7d44a4a6a',1,'SChannel']]]
];
